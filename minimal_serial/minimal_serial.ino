
#include <SoftwareSerial.h>
SoftwareSerial soft_serial(7,8); // DYNAMIXELShield UART RX/TX
#define DXL_SERIAL   Serial
#define DEBUG_SERIAL soft_serial

#define MAXVALUESZ 64

char tmp[MAXVALUESZ];

void setup() {

  DEBUG_SERIAL.begin(19200);
  
  //Wait until the serial port for terminal is opened
  while(!DEBUG_SERIAL);
  
  DEBUG_SERIAL.listen();
  DEBUG_SERIAL.println("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");

}

void loop() {
  int ch;
  if(DEBUG_SERIAL.available()) {
    ch = DEBUG_SERIAL.read();
    DEBUG_SERIAL.write(ch);
    delay(100);
  }
}
