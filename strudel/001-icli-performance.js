Pattern.prototype.dance = function () {
  const pat = this;
  const speechPat = this.withHap((hap) => {
    return hap.withValue((value) => {
      var message = '';
      var action = value.action;
      if ('action' in value) {
        message = value.action;
        if ('part' in value) {
          message += ' your ' + value.part;
        }
        if (action == 'move' && 'to' in value) {
          const perc = Math.floor(value.to * 100);
          message += ' to ' + perc + 'percent ';
        }
        if ('by' in value) {
          const by = Math.floor(value.by * 100);
          message += ' by ' + by + 'percent ';
        }
        if ('dur' in value) {
          const dur = value.dur / 1000;
          message += ' over ' + dur + 'seconds';
        }
      }
      return message;
    });
  });
  return stack(
    speechPat.speak("[en-US|en-GB]", "[0|1]"),
    pat.serial(38400, true, true)
  );
};

var { action, to, part, by, dur } = createParams('action', 'to', 'part', 'by');

window.a = slowcat(
  action("switch").part("head").dur(2000)

  //action('reset')

  //action('switch')
  // .part('waist')
  //.dur(250)

  //  action('reset'),
  // action('move')
  // .part("<back head waist>")
  // .to(slowcat(0.25,0.75))
  //action('reset')

  //repeat head

  //action('switch')
  //.part('waist')
  //.dur(250),
  // action('lean')
  // .part( stack ('back', 'head'))
  // .by(0.33)
  // .dur(choose(1500,2000, 250)),
  //action('switch')
  // .part('head')
  //part(stack('waist', 'head', 'back'))
  //  .action('move')
  //  .to(0.5)
  //part(stack('head'))
  //.action(choose('move','lean'))
  //.to(choose(0.25, 0.5, 0.33))

  //action('reset')
  //action('move').part('waist').to(0.25),
  //action('switch').part('waist').dur(5000)
)
  .dance()
  .slow(8);

stack(window.a, window.b);

//silence
