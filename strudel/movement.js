var {action, to, part, by, dur, waist, back, head} =
    createParams('action', 'to', 'part', 'by', 'dur', 'waist', 'back', 'head');

function move(w, b, h, f) {
  const parts =
	waist(f.mul(w)).back(f.mul(b)).head(f.mul(h));
  return(parts);
}

const moves = {
  sway: move(0.25,0.125,1,sine2),
  wiggle: move(0,0,1,sine2.mul(0.5).slow(1).add(sine2.mul(0.05).fast(24))),
  bow: move(0,-1,0,tri),
  nod: move(0,-0.25,0,fastcat(tri).fast(3)),
  diagonaltwist: move(1,1,1,sine2)
}

Pattern.prototype.robot = function() {
  const f = function(hap) {
    const result = [];
    for (var k of Object.keys(hap.value)) {
      var v = hap.value[k];
      result.push(hap.withValue(_ => ({
        to: ((v + 1) / 2).toFixed(2), 
        part: k, 
        action: 'move'
       })
      ));
    }
    return result;
  }; 

  return this.withHaps(haps => flatten(haps.map(f))) //.serial(38400);
}

function inhabit(lookup, pat) {
    return pat.fmap(v => v in lookup ? lookup[v] : pure(silence)).squeezeJoin()
}

function g(pat) {
  return inhabit(moves, pat)
}

const stand = pure({head: 0, waist: 0, back: 0});

const thing = g("diagonaltwist nod").add(g("wiggle").slow(2.5))
  .slow(8).struct("1*32").robot();

console.log(thing._firstCycleValues)

                       
thing.serial(38400, true);
// silence
