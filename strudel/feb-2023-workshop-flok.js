//// a

slowcat(
  action("move").part("head").to(0.25),
  action("switch").part("head").dur(2000),
  action("lean").part("waist").dur(2000),
  action("switch").part("waist").dur(2000),
  action("move").part("head").to(0.25),
  action("switch").part("head").dur(1000),
  action("lean").part("waist").dur(1000),
  action("switch").part("waist").dur(1000),
  action("reset")
  //action('switch').part('waist').dur(5000)
)
  .slow(3)
  .dance();

// action('reset').dance()

//// b

patternMove("sway nod diagonaltwist".slow(8).every(2, rev));

// sum(wiggle, nod).slow(2).splitSerial()

//// c

await get_drawing("kate"); // https://slab.org/tmp/draw/

window.moves = {
  sway: move(
    0.25, // amount of waist movement
    0.125, // amount of back movement
    -1, // amount of head movement
    sine2 // the movement 'shape'
  ),
  wiggle: move(0, 0, 1, sine2.mul(0.5).slow(1).add(sine2.mul(0.05).fast(24))),
  bow: move(0, -1, 0, tri),
  nod: move(0, -0.25, 0, fastcat(tri, saw).fast(3)),
  diagonaltwist: move(1, 1, 1, sine2),
  draw: move(0, 0, 1, kate),
};

window.patternMove = (pat) => inhabit(moves, pat);

for (var key in window.moves) {
  window[key] = window.moves[key];
}

action("velocity").to(50).serial(38400);

//// d

var parts = ['waist', 'back', 'head'];

var { action, to, part, by, dur, waist, back, head } = createParams(
  'action',
  'to',
  'part',
  'by',
  'dur',
  'waist',
  'back',
  'head'
);
window.action = action;
window.to = to;
window.part = part;
window.by = by;
window.dur = dur;
window.waist = waist;
window.back = back;
window.head = head;

Pattern.prototype.dance = function () {
  const pat = this;
  const speechPat = this.withHap((hap) => {
    return hap.withValue((value) => {
      var message = '';
      var action = value.action;
      if ('action' in value) {
        message = value.action;
        if ('part' in value) {
          message += ' your ' + value.part;
        }
        if (action == 'move' && 'to' in value) {
          const perc = Math.floor(value.to * 100);
          message += ' to ' + perc + 'percent ';
        }
        if ('by' in value) {
          const by = Math.floor(value.by * 100);
          message += ' by ' + by + 'percent ';
        }
        if ('dur' in value) {
          const dur = value.dur / 1000;
          message += ' over ' + dur + 'seconds';
        }
      }
      return message;
    });
  });
  return stack(
    //speechPat.speak('[en-US|en-GB]', '[0|1]'),
    pat.serial(38400, true, true)
  );
};

await import('https://dweet.io/client/dweet.io.min.js');

window.get_drawing = function (name) {
  console.log('getting ' + 'alpaca-draw-' + name);
  dweetio.get_latest_dweet_for('alpaca-dance-' + name, function (err, dweet) {
    var dweet = dweet[0];
    console.log(dweet.thing);
    console.log(dweet.content);
    console.log(dweet.created);
    var vs = [];

    for (var i = 0; i < 200; ++i) {
      vs.push(dweet.content[i] >= 0 ? dweet.content[i] : silence);
    }
    console.log(vs);
    window[name] = sequence(...vs).toBipolar();
  });
};

/*
window.move = function(waist, back, head, f) {
  const segments = 128;
  const parts1 = stack(
    to(f.segment(segments).mul(waist).fromBipolar()).part('waist'),
    to(f.segment(segments).mul(back).fromBipolar()).part('back'),
    to(f.segment(segments).mul(head).fromBipolar()).part('head')
  ).action('move');
  return parts1;
}
*/

Pattern.prototype.splitUnipolarBodyParts = function () {
  const pat = this;
  const pats = [];
  for (const prt of parts) {
    pats.push(
      pat.fmap((x) => ({ action: 'move', part: prt, to: (x[prt] + 1) / 2 }))
    );
  }
  return stack(...pats);
};

window.move = function (w, b, h, f) {
  const parts1 = waist(f.mul(w)).back(f.mul(b)).head(f.mul(h));
  return parts1;
};

window.inhabit = function (lookup, pat) {
  return pat
    .fmap((v) => (v in lookup ? lookup[v] : pure(silence)))
    .squeezeJoin()
    .splitUnipolarBodyParts()
    .segment(32)
    .serial(38400, true, true);
};

window.sum = (...pats) =>
  pats.reduce((a, b) => a.add(b), steady({ waist: 0, back: 0, head: 0 }));

Pattern.prototype.splitSerial = function () {
  return this.splitUnipolarBodyParts().segment(32).serial(38400, true, true);
};

silence;
