
function sketch(p) {
    const waist = []
    const back = []
    const head = []
    window.robot_coords = {waist: waist, back: back, head: head};
    const timewindow = 32;
    const w = 4;
    const s = 200;
    const random_fill = false;

    p.setup = function() {
	p.colorMode(p.HSL);

        const canvas = getDrawContext().canvas;
        p.createCanvas(canvas.width, canvas.height, p.P2D, canvas);
	//p.createCanvas(s,s*4);

	if (random_fill) {
	    for (let i = 0; i < 100; ++i) {
		const t = (i / 100) * timewindow;
		waist.push({t: t, value: Math.random()});      
		back.push({t: t, value: Math.random()});      
		head.push({t: t, value: Math.random()});
	    }
	}
    }

    function clean (a) {
	const t = window.currentT;
	while (a.length > 0 && a[0].t <= t - timewindow) {
	    a.shift();
	}
    }

    function vis(a, colour) {
	const t = window.currentT;
	p.stroke(colour);
	p.strokeWeight(0.01);
	p.noFill();
	p.beginShape();
	for (let i = 0; i < a.length; ++i) {
	    const pos = (1-((t-a[i].t)/timewindow))*w;
	    p.vertex(a[i].value,pos);
	}
	p.endShape();
    }

    p.draw = function() {
	p.background(50);
	p.scale(s);
	p.translate(5,0);
	clean(waist); clean(back); clean(head);
	
	vis(waist, p.color(10,80,40));
	vis(back, p.color(90,80,40));
	vis(head, p.color(200,80,40));
    }
}

window.inited = window.inited ?? false;
if(!window.inited) {
  window.inited = true;
  await import('https://cdn.jsdelivr.net/npm/p5@1.9.0/lib/p5.js');
  new p5(sketch)
}

Pattern.prototype.graph = function () {
    return this.withHap((hap) => {
      const existing = hap.context.onTrigger;
      const onTrigger = (time, hap, currentTime, ...args) => {
          window.currentT = currentTime;

	  if (existing) {
	      existing(time, hap, currentTime, ...args);
	  }
	  if (hap.value.action === 'move') {
	      window.robot_coords[hap.value.part].push({t: time, value: hap.value.to});
	  }
      };
      return hap.setContext({ ...hap.context, onTrigger, dominantTrigger: false });
  });
};
