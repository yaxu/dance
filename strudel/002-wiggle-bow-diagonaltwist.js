var { action, to, part, by, dur } = createParams(
  'action',
  'to',
  'part',
  'by',
  'dur'
);

function move(waist, back, head, f) {
  const segments = 128;
  const parts1 = stack(
    to(f.segment(segments).mul(waist).fromBipolar()).part("waist"),
    to(f.segment(segments).mul(back).fromBipolar()).part("back"),
    to(f.segment(segments).mul(head).fromBipolar()).part("head")
  ).action("move");
  return parts1;
}

const moves = {
  sway: move(0.25, 0.125, 1, sine2),
  wiggle: move(0, 0, 1, sine2.mul(0.5).slow(1).add(sine2.mul(0.05).fast(24))),
  bow: move(0, -1, 0, tri),
  nod: move(0, -0.25, 0, fastcat(tri, saw).fast(3)),
  diagonaltwist: move(1, 1, 1, sine2),
};

function inhabit(lookup, pat) {
  return pat
    .fmap((v) => (v in lookup ? lookup[v] : pure(silence)))
    .squeezeJoin();
}

inhabit(moves, "wiggle bow diagonaltwist").slow(16).serial(38400, true, true);

//action("reset").serial(38400, true, true)
