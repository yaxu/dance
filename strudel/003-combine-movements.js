var parts = ['waist', 'back', 'head'];

var { action, to, part, by, dur, waist, back, head } = createParams(
  'action',
  'to',
  'part',
  'by',
  'dur',
  'waist',
  'back',
  'head'
);

Pattern.prototype.splitUnipolarBodyParts = function() {
  const pat = this;
  const pats = [];
  for (const prt of parts) {
    pats.push(pat.fmap(x => ({action: 'move',
                              part: prt, 
                              to: (x[prt] + 1) / 2
                             })
                       )
             );
  }
  return stack(...pats);
}

const sum = (...pats) => pats.reduce((a,b) => a.add(b), 
                                     steady({waist: 0, back: 0, head: 0})
                                    )

function move(w, b, h, f) {
  const parts1 = 
    waist(f.mul(w))
    .back(f.mul(b))
    .head(f.mul(h));
  return parts1;
}

const moves = {
  sway: move(0.25, 0.125, 1, sine2),
  wiggle: move(0, 0, 1, sine2.mul(0.5).slow(1).add(sine2.mul(0.05).fast(24))),
  bow: move(0, -1, 0, tri),
  nod: move(0, -0.25, 0, fastcat(tri, saw).fast(3)),
  diagonaltwist: move(1, 1, 1, sine2),
};

function inhabit(lookup, pat) {
  return pat
    .fmap((v) => (v in lookup ? lookup[v] : pure(silence)))
    .squeezeJoin();
}

//console.log(sum(move(0,0,1,saw), move(0,1,0,saw), move(1,0,0,saw)).firstCycleValues)

console.log(sum(moves['sway']).firstCycleValues)
console.log(moves['sway'].firstCycleValues)

var pat = sum(moves['wiggle'], moves['nod'])
  .slow(2)

pat.splitUnipolarBodyParts().segment(32).serial(38400, true, true);

//action("reset").serial(38400, true, true)
