
/*******************************************************************************
  Copyright 2016 ROBOTIS CO., LTD.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*******************************************************************************/

// limits..
// a 170 - 850 (if nose is straight)
// a 170 - 810 (if nose is left)
// a 200 - 850 (if nose is right)
// b 160 - 850
// c 0 - 900 (gets a bit caught)
// slice 810a200c - 200a810c

#include <Dynamixel2Arduino.h>
//#include <NeoSWSerial.h>
//#include <SoftwareSerial.h>

//NeoSWSerial ss(10, 11);
//SoftwareSerial ss(10, 11);

#define DXL_SERIAL Serial

// #define DEBUG_SERIAL ss
#define DEBUG_SERIAL Serial1

#define DEBUG_BAUD 115200

const uint8_t DXL_DIR_PIN = 2;

Dynamixel2Arduino dxl(DXL_SERIAL, DXL_DIR_PIN);

#define PRESENT_POSITION_ADDR     36
#define PRESENT_POSITION_ADDR_LEN 2
#define PRESENT_LOAD_ADDR         40
#define PRESENT_LOAD_ADDR_LEN     2
#define PRESENT_SPEED_ADDR         38
#define PRESENT_SPEED_ADDR_LEN     2
#define TIMEOUT 10 // ms

#define VERBOSE 0

#define MAX_MSG_SZ 128
#define HISTORY 2

char msg[MAX_MSG_SZ];
int msg_n = 0;

char history[HISTORY][MAX_MSG_SZ];
int history_n = 0;

char lookahead_char = '\0';
int lookahead = 0;

const int joints = 3;

uint16_t present_position[joints] = {0, 0, 0};
uint16_t present_load[joints]     = {0, 0, 0};
uint16_t present_speed[joints]    = {0, 0, 0};
uint16_t target_position[joints]  = {0, 0, 0};

const uint8_t DXL_ID = 1;
const float DXL_PROTOCOL_VERSION = 1.0;

float velocity = 100;

#define MAXPARAMS 16
#define MAXVALUESZ 64
char tmp[MAXVALUESZ];

unsigned long start_time;

enum action {
  no_action,
  move_action,
  halve_action,
  unhalve_action
};

typedef struct {
  int from;
  int to;
  int duration;
  int joint;
} move_info_t;

typedef struct {
  action act;
  unsigned long start;
  union {
    move_info_t move_info;
  } info;
} action_t;

#define MAX_ACTIONS 8

int good = 0;
int bad = 0;

action_t actions[MAX_ACTIONS];

uint16_t crc16(char* pData, int length) {
  uint8_t i;
  uint16_t wCrc = 0xffff;

  // sprintf(tmp, "checking %d chars of %s\n", length, pData);
  // DEBUG_SERIAL.println(tmp);
    
  while (length--) {
    wCrc ^= *(unsigned char *)pData++ << 8;
    for (i = 0; i < 8; i++)
      wCrc = wCrc & 0x8000 ? (wCrc << 1) ^ 0x1021 : wCrc << 1;
  }
  return wCrc & 0xffff;
}

uint16_t crc16_msg(char *pData) {
  for (int i = 0; pData[i] |= '\0'; ++i) {
    if (pData[i] == ')') {
      return(crc16(pData, i+1));
    }
  }
  return(0);
}

void setup() {
  start_time = millis();
  for (int i = 0; i < MAX_ACTIONS; ++i) {
    actions[i].act = no_action;
  }
  for (int i = 0; i < HISTORY; ++i) {
    history[i][0] = '\0';
  }
  pinMode(10, INPUT_PULLUP);

  DEBUG_SERIAL.begin(DEBUG_BAUD);

  //DEBUG_SERIAL.listen();
  DEBUG_SERIAL.println("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!");

  dxl.begin(1000000);
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);

  for (int i = 1; i <= joints; ++i) {
    dxl.ping(i);
    dxl.torqueOff(i);
    dxl.setOperatingMode(i, OP_POSITION);
    dxl.torqueOn(i);
    dxl.setGoalVelocity(i, 15.0, UNIT_PERCENT);
  }

  sense();

  for (int i = 0; i < joints; ++i) {
    move(i + 1, 300);

    target_position[i ] = 300;
  }
}

int percentToPos(int joint_id, float percent) {
  int result;
  switch (joint_id) {
    case 1: result = floor((percent * 680.0f) + 170.0f); break;
    case 2: result = floor((percent * 690.0f) + 160.0f); break;
    default: result = floor((percent * 676.0f) + 174.0f);
  }

  if (VERBOSE > 1) {
    sprintf(tmp, "percentToPos from ");
    DEBUG_SERIAL.print(tmp);
    dtostrf(percent, 7, 3, tmp);
    DEBUG_SERIAL.print(tmp);
    sprintf(tmp, " to %d", result);
    DEBUG_SERIAL.println(tmp);
  }

  return result;
}

float posToPercent(int joint_id, int pos) {
  float result;
  float fpos = (float) pos;
  switch (joint_id) {
    case 1: result = (fpos - 170.0f) / 680.0f; break;
    case 2: result = (fpos - 160.0f) / 690.0f; break;
    default: result = (fpos - 174.0f) / 676.0f; break;
  }

  if (VERBOSE > 1) {
    sprintf(tmp, "posToPercent from %d to", pos);
    DEBUG_SERIAL.print(tmp);
    dtostrf(result, 7, 3, tmp);
    DEBUG_SERIAL.println(tmp);
  }
  return result;
}

void move(int id, int pos) {
  dxl.setGoalVelocity(id, velocity, UNIT_PERCENT);
  dxl.setGoalPosition(id, pos);
}

void sense() {
  uint16_t value;
  for (int i = 0; i < joints; ++i) {
    dxl.read(i + 1, PRESENT_POSITION_ADDR, PRESENT_POSITION_ADDR_LEN, (uint8_t*)&value, sizeof(value), TIMEOUT);
    present_position[i] = value;
    dxl.read(i + 1, PRESENT_LOAD_ADDR, PRESENT_LOAD_ADDR_LEN, (uint8_t*)&value, sizeof(value), TIMEOUT);
    present_load[i] = value;
    dxl.read(i + 1, PRESENT_SPEED_ADDR, PRESENT_SPEED_ADDR_LEN, (uint8_t*)&value, sizeof(value), TIMEOUT);
    present_speed[i] = value;
  }
}

void update_move(action_t *action) {
  unsigned long now = millis();
  move_info_t *info = &action->info.move_info;

  float perc = ((float) (now - action->start)) / ((float) info->duration);
  if (perc >= 1) {
    action->act = no_action;
  }
  else {
    int pos = info->from + floor(((float) (info->to - info->from)) * perc);
    move(info->joint, pos);
  }
}

void update() {
  for (int i = 0; i < MAX_ACTIONS; ++i) {
    switch (actions[i].act) {
      case move_action:
      case halve_action:
      case unhalve_action:
        update_move(&actions[i]);
        break;
    }
  }
}

// returns the index of the first inactive 'action', or if they're all active,
// the one active the longest
action_t *empty_action() {
  action_t *result = &actions[0];
  if (result->act != no_action) {
    for (int i = 1; i < MAX_ACTIONS; ++i) {
      if (actions[i].act == no_action) {
        result = &actions[i];
        break;
      }
      if (actions[i].start < result->start) {
        result = &actions[i];
      }
    }
  }
  return (result);
}

int joint_name_to_id(char *name) {
  // if (strncmp(name, "waist", MAXVALUESZ) == 0) {
  if (name[0] == 'w') {
    return 1;
  }
  // if (strncmp(name, "back", MAXVALUESZ) == 0) {
  if (name[0] == 'b') {
    return 2;
  }
  // if (strncmp(name, "head", MAXVALUESZ) == 0) {
  if (name[0] == 'h') {
    return 3;
  }
  // default
  return (1);
}

char *lookup(char *args[MAXPARAMS], int argn, char key, char *default_value) {
  for (int i = 0; i < (argn - 1); i += 2) {
    if (args[i][0] == key) {
      return (args[i + 1]);
    }
  }
  return (default_value);
}

void action_status() {
  char str[128];
  sprintf(str, "Good: %d Bad: %d", good, bad);
  DEBUG_SERIAL.println(str);
  DEBUG_SERIAL.print("history: \n");
  for (int i = 0; i < HISTORY; ++i) {
    DEBUG_SERIAL.print(i);
    DEBUG_SERIAL.print(": ");
    DEBUG_SERIAL.println(history[i]);
  }
  for (int i = 0; i < MAX_ACTIONS; ++i) {
    if (actions[i].act == no_action) {
      DEBUG_SERIAL.println("no_action");
    }
    if (actions[i].act == move_action) {
      DEBUG_SERIAL.println("switch_action");
    }
    if (actions[i].act == halve_action) {
      DEBUG_SERIAL.println("halve_action");
    }
    if (actions[i].act == unhalve_action) {
      DEBUG_SERIAL.println("unhalve_action");
    }
  }

  if (VERBOSE > 1) {
    sense();
    sprintf(tmp, "positions: %d, %d, %d", present_position[0], present_position[1], present_position[2]);
    DEBUG_SERIAL.println(tmp);
  }
}

void act(char *command, char *args[MAXPARAMS], int argn) {
  if (VERBOSE > 1) {
    DEBUG_SERIAL.print(command);
    DEBUG_SERIAL.print("(");
    DEBUG_SERIAL.print(args[0]);
    DEBUG_SERIAL.println(")");
  }

  // a 170 - 850 (if nose is straight)
  // a 170 - 810 (if nose is left)
  // a 200 - 850 (if nose is right)
  // b 160 - 850
  // c 124 - 900 (gets a bit caught)

  //if (strncmp("velocity", command, MAXVALUESZ) == 0) {
  if (command[0] == 'v') {
    velocity = atof(lookup(args, argn, 't', (char *) "0.5"));
    if (VERBOSE > 1) {
      sprintf(tmp, "set velocity %s", args[0]);
      DEBUG_SERIAL.println(tmp);
    }
  }
  //else if (strncmp("move", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'm') {
    float pos = atof(lookup(args, argn, 't', (char *) "0.5"));
    float panx = atof(lookup(args, argn, 'x', (char *) "0.5"));

    pos = (pos * 2) - 1;

    //panx = 1 - panx;
    
    panx *= 2;
    if (panx > 1) {
      panx = 1;
    }
    
    pos *= panx;

    pos = (pos + 1)/2;
    
    int joint = joint_name_to_id(lookup(args, argn, 'p', (char *) "waist"));
    int posi = percentToPos(joint, pos);
    if (VERBOSE > 1) {
      sprintf(tmp, "move %d to %d pan %f", joint, posi, panx);
      DEBUG_SERIAL.println(tmp);
    }
    move(joint, posi);
    target_position[joint - 1] = posi;
  }
  //else if (strncmp("move2", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'o') { // for 'move with override'
    dxl.setGoalVelocity(1, 100, UNIT_PERCENT);
    sense();

    float to_percent = atof(lookup(args, argn, 't', (char *) "0.5"));
    int joint = joint_name_to_id(lookup(args, argn, 'p', (char *) "waist"));
    int duration = atoi(lookup(args, argn, 'd', (char *) "1000"));

    for (int i = 0 ; i < MAX_ACTIONS; ++i) {
      // cancel movements of the same joint
      if (actions[i].act == move_action && actions[i].info.move_info.joint == joint) {
        actions[i].act = no_action;
      }
    }

    action_t *action = empty_action();
    action->act = move_action;

    int from = present_position[joint - 1];
    int to = percentToPos(joint, to_percent);;

    action->start = millis();
    action->info.move_info.from = from;
    action->info.move_info.to = to;
    action->info.move_info.duration = duration;
    action->info.move_info.joint = joint;
    target_position[joint - 1] = to;
  }
  //else if (strncmp("reset", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'r') {
    for (int joint = 1; joint <= joints; ++joint) {
      move(joint, 512);
      target_position[joint - 1] = 512;
    }
  }
  //else if (strncmp("switch", command, MAXVALUESZ) == 0) {
  else if (command[0] == 's') {
    dxl.setGoalVelocity(1, 100, UNIT_PERCENT);
    sense();

    int joint = joint_name_to_id(lookup(args, argn, 'p', (char *) "waist"));
    int duration = atoi(lookup(args, argn, 't', (char *) "1000"));

    for (int i = 0 ; i < MAX_ACTIONS; ++i) {
      // cancel movements of the same joint
      if (actions[i].act == move_action && actions[i].info.move_info.joint == joint) {
        actions[i].act = no_action;
      }
    }

    action_t *action = empty_action();
    action->act = move_action;



    //int from = present_position[joint - 1];
    int from = target_position[joint - 1];
    int to = (0 - (from - 512)) + 512;

    if (VERBOSE > 0) {
      sprintf(tmp, "switch %d from %d to %d in %d (%s)", joint, from, to, duration, args[0]);
      DEBUG_SERIAL.println(tmp);
    }

    action->start = millis();
    action->info.move_info.from = from;
    action->info.move_info.to = to;
    action->info.move_info.duration = duration;
    action->info.move_info.joint = joint;
    target_position[joint - 1] = to;
  }
  //else if (strncmp("lean", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'l') {    
    dxl.setGoalVelocity(1, 100, UNIT_PERCENT);
    sense();
    action_t *action = empty_action();
    action->act = move_action;

    int joint = joint_name_to_id(lookup(args, argn, 't', (char *) "waist"));
    float by = atof(lookup(args, argn, 'b', (char *) "0.5"));
    int duration = atoi(lookup(args, argn, 'd', (char *) "1000"));

    int from = present_position[joint - 1];
    float from_percent = posToPercent(joint, from);
    float to_percent;
    if (from_percent < 0.5) {
      to_percent = from_percent * (1 - by);
    }
    else {
      to_percent = from_percent + ((1 - from_percent) * by);
    }

    int to = percentToPos(joint, to_percent);

    if (VERBOSE > 1) {
      sprintf(tmp, "lean %d from %d to %d in %d (%s)", joint, from, to, duration, args[0]);
      DEBUG_SERIAL.println(tmp);
    }

    action->start = millis();
    action->info.move_info.from = from;
    action->info.move_info.to = to;
    action->info.move_info.duration = duration;
    action->info.move_info.joint = joint;
    target_position[joint - 1] = to;
  }
  //else if (strncmp("halve", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'h') {
    sense();
    action_t *action = empty_action();
    action->act = halve_action;

    int joint = joint_name_to_id(lookup(args, argn, 'p', (char *) "waist"));
    int duration = atoi(lookup(args, argn, 'd', (char *) "1000"));

    int from = present_position[joint - 1];
    int to = ((from - 512) / 2) + 512;

    if (VERBOSE > 1) {
      sprintf(tmp, "halve from %d to %d in %d (%s)", from, to, duration, args[0]);
      DEBUG_SERIAL.println(tmp);
    }

    action->info.move_info.from = from;
    action->info.move_info.to = to;
    action->start = millis();
    action->info.move_info.duration = duration;
    action->info.move_info.joint = joint;
    target_position[joint - 1] = to;
  }
  //else if (strncmp("unhalve", command, MAXVALUESZ) == 0) {
  else if (command[0] == 'u') {
    sense();
    action_t *action = empty_action();
    action->act = halve_action;

    int joint = joint_name_to_id(lookup(args, argn, 'p', (char *) "waist"));
    int duration = atoi(lookup(args, argn, 'd', (char *) "1000"));

    int from = present_position[joint - 1];
    int to = ((from - 512) / 2) + 512;

    if (VERBOSE > 1) {
      sprintf(tmp, "halve from %d to %d in %d (%s)", from, to, duration, args[0]);
      DEBUG_SERIAL.println(tmp);
    }

    action->info.move_info.from = from;
    action->info.move_info.to = to;
    action->start = millis();
    action->info.move_info.duration = duration;
    action->info.move_info.joint = joint;
    target_position[joint - 1] = to;
  }
  //else if (strncmp("status", command, MAXVALUESZ) == 0) {
  else if (command[0] == '?') {
    action_status();
  }
  //else if (strncmp("test", command, MAXVALUESZ) == 0) {    
  else if (command[0] == 't') {   
    sprintf(tmp, "percentToPos(0.5) = %d", percentToPos(1, 0.5));
    DEBUG_SERIAL.println(tmp);
    sprintf(tmp, "percentToPos(0.25) = %d", percentToPos(1, 0.25));
    DEBUG_SERIAL.println(tmp);
    sprintf(tmp, "percentToPos(0.75) = %d", percentToPos(1, 0.75));
    DEBUG_SERIAL.println(tmp);
  }
  else {
    if (VERBOSE > 1) {
      DEBUG_SERIAL.print("unrecognised command ");
      DEBUG_SERIAL.print(command);
    }
  }
}

void parse() {
  char command[MAXVALUESZ + 1];
  char parameters[MAXVALUESZ + 1];  
  char *args[MAXPARAMS];
  int argn = 0;
  
  int crc_failed = 0;
  
  uint16_t chk = crc16_msg(msg);
      
  snprintf(history[(history_n++) % HISTORY], MAX_MSG_SZ, "[ :%u:%c%c] %s", chk, (chk >> 8) & 0xFF, chk & 0xFF, msg);

  int found_command = 0;

  int i;
  for (i=0; msg[i] != '\0'; ++i) {
    if (msg[i] == '(') {
      command[i] = '\0';
      found_command = 1;
      break;
    }
    command[i] = msg[i];
  }

  if (!found_command) {
    // bad
    return;
  }

  i++;

  if (msg[i] != ')' && msg[i] != '\0') {
    args[0] = msg + i;
    argn++;
    while(msg[i] != ')' && msg[i] != '\0') {
      // treat , and : as the same, making a flat list.
      if (msg[i] == ',' || msg[i] == ':') {
        msg[i] = '\0';
        args[argn++] = msg + i + 1;
      }
      ++i;
    }
  }
  msg[i] = '\0';
  if (VERBOSE > 1) {
    sprintf(tmp, "parameter count: %d", argn);
    DEBUG_SERIAL.println(tmp);
    for (int j = 0; j < argn; j++) {
      sprintf(tmp, "parameter %d: %s", j, args[j]);
      DEBUG_SERIAL.println(tmp);
    }
  }

  ++i;

  if (msg[i] == '|') {
    // found a crc
    char a = msg[i+1];
    if (a == '\0') return; // bad
    
    char b = msg[i+2];
    if (b == '\0') return; // bad

    char achk = (chk >> 8) & 0xFF;
    char bchk = chk & 0xFF;
    
    if ((a == achk) && (b == bchk)) {
      good++;
      history[(history_n-1) % HISTORY][1] = 'g';
    }
    else {
      crc_failed = 1; // skip message processing.
      // crc error.. ignore
      bad++;
      // sprintf(history[(history_n-2) % HISTORY], "%d vs %d / %d vs %d (%u)", a, achk, b, bchk, chk);    
    }
  }
  
  if (!crc_failed) {
    act(command, args, argn);
  }
}

void loop() {
  int ch;
  while (DEBUG_SERIAL.available()) {
    ch = DEBUG_SERIAL.read();
    if (ch == ';') {
      msg[msg_n] = '\0';      
      msg_n = 0;
      parse();
    }
    else {
      msg[msg_n++] = ch;
    }
  }

  update();
}
