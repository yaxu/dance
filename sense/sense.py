#!/usr/bin/python3

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import statsmodels.api as sm
import numpy as np
from scipy.signal import find_peaks
import threading
import random
import time
import re
import liblo
import sys
import statistics
import math
import link

osc_targets = {}

names = ['alex', 'kate']

try:
    #osc_target = liblo.Address("localhost", 57121)
    #liblo.send(osc_target, "/hello")
    osc_targets['kate'] = liblo.Address("SOA-KINE-DPOANX-0-FACSTF-ksicchio-XY46FQL9V0.local", 57121)
    liblo.send(osc_targets['kate'], "/hello")
    
    osc_targets['alex'] = liblo.Address("127.0.0.1", 57121)
    liblo.send(osc_targets['alex'], "/hello")
    
except (liblo.AddressError, err):
    print(str(err))
    sys.exit()


# x,y,z..
dimensions = 1

confidence = 0.5

prev = {}

for name in names:
    prev[name] = []

samplerate = 20

window = 4 # in seconds

bpc = 2 # beats per cycle

# Initialise link with default tempo of 120
linkclock = link.Link(120)
linkclock.enabled = True

# wait a moment for link to sort itself out
time.sleep(2)
# s = linkclock.captureSessionState()

cpses = {}
for name in names:
    cpses[name] = (52 / bpc) / 60
    print("%s starting cps: %f" % (name, cpses[name]))

def setTempo(name, target, maxchange):
    new_cps = target
    my_cps = cpses[name]
    
    # maximum value to change cps by (smoothing)
    maxchange = maxchange / samplerate
    
    #bpm = state.tempo()
    #cps = (bpm / 60) / bpc

    change = new_cps - my_cps
    
    if abs(change) > maxchange:
        if change > 0:
            new_cps = my_cps + maxchange
        else:
            new_cps = my_cps - maxchange
    else:
        new_cps = my_cps + change

    new_bpm = new_cps*60*bpc

    cpses[name] = new_cps

    print("%s new cps: %.3f" % (name, cpses[name]))
    
    # print("set cps: %.2f target: %.2f old bpm: %.2f new bpm: %.2f" % (new_cps, target, bpm, new_bpm))
    
    liblo.send(osc_targets[name], "/cps", float(new_cps))
    
    state = linkclock.captureSessionState()
    state.setTempo(new_bpm, linkclock.clock().micros());
    linkclock.commitSessionState(state);

try:
    osc_server = liblo.Server(8000)
except liblo.ServerError as err:
    print(err)
    sys.exit()

history = {}

def smooth(name, value):
    window = 5
    if not (name in history):
        history[name] = []
    history[name].append(value)
    result = sum(history[name]) / len(history[name])
    if len(history[name]) >= window:
        history[name].pop(0)
    return result

def incoming(self, name, value):
  value = smooth(name,value)
  total = 0.0
  maximum = 0.0
  signal_freqs = []
  cps_value = -1

  i = names.index(name)

  self._data.XData[i].append(self._data.XData[i][-1] + 1)
  self._data.YData[i].append(value)

  if (len(self._data.YData[i]) > (window * samplerate)):
      # remove first frequency
      self._data.YData[i].pop(0)
      self._data.XData[i].pop(0)
      
      x = self._data.YData[i]
      t = self._data.XData[i]
      
      # https://www.statsmodels.org/dev/generated/statsmodels.tsa.stattools.acf.html
      # "if alpha=.05, 95 % confidence intervals
      # are returned where the standard
      # deviation is computed according to
      # Bartlett”s formula."
      auto = sm.tsa.acf(x, nlags=2000, alpha=.05)
      
      self._data.mags[i] = auto[0]
      self._data.conf[i] = auto[1][:, 1]

      # find peaks in autocorrelation
      peaks = find_peaks(auto[0])[0]
      
      if len(peaks) > 0:
          # find variance
          variance = 0
          try:
              variance = statistics.variance(auto[0])
              # print("variance: %.2f" % variance)
          except:
              pass

          rnge = 0
          last_second = x[0-math.floor(samplerate):]
          rnge = max(last_second) - min(last_second)
          
          # Ignore if range is low - performer isn't moving much
          if rnge > 0.25:
              lag = -1
              # find first peak with a confidence of 1.2 or greater
              for peak in peaks:
                  if self._data.conf[i][peak] >= confidence:
                      lag = peak
                      #print("sensor %d cps %.2f conf %.2f range %.2f" % (i, 1/(peak/samplerate), self._data.conf[i][peak], rnge))
                      break
              if lag > -1:
                  self._data.peakxy[i] = (lag/samplerate,self._data.mags[i][lag])
                  cps_value = 1/(lag/samplerate)
      else:
        self._data.peakxy[i] = (0,0)
                            
      # Time axis
      self._data.freqs[i] = list(map(lambda x: x / samplerate, range(0,len(self._data.mags[i]))))

  if cps_value > 0:
    # print("set cps: %f" % cps_value)
    setTempo(name, cps_value, 0.4)
    print("sending")
    dur = (1 / samplerate)*cpses[name]
    print("dur: %s" % dur)
    liblo.send(osc_targets[name], "/move", name, value, dur/2)

class Data():

    def __init__(self):
        def makelist(value):
            result = []
            for i in range(0, len(names)):
                result.append([value])
            return result
        self.XData = makelist(0)
        self.YData = makelist(0)
        self.freqs = makelist(0)
        self.mags  = makelist(0)
        self.conf  = makelist(0)
        self.peakxy  = []
        for i in range(0, len(names)):
            self.peakxy.append((2,1))

class Plot():
    def __init__(self, data):
        cols = len(names)
        self._data = data
        fig, axs = plt.subplots(3,cols)
        fig.suptitle("All the numbers")
        self.fftline = []
        self.sigline = []
        self.confline = []
        self.annotation = []
        
        for i in range(0,cols):
            confline, = axs[0,i].plot(0, 0)
            self.confline.append(confline)
            
            fftline, = axs[1,i].plot(0, 0)
            self.fftline.append(fftline)

            annotation = axs[1,i].annotate(
                'local max', xy=(2, 1), xytext=(3, 1.5),
                arrowprops=dict(facecolor='black', shrink=0.05),
            )
            self.annotation.append(annotation)
            
            sigline, = axs[2,i].plot(0, 0)
            self.sigline.append(sigline)
            
        self.ani = FuncAnimation(plt.gcf(), # get current figure
                                 self.run,
                                 interval = 200,
                                 repeat=True
                                 )

    def run(self, x):  
        #print("plotting data")
        cols = len(names)
        for i in range(0,cols):
            self.sigline[i].set_data(self._data.XData[i], self._data.YData[i])
            self.fftline[i].set_data(self._data.freqs[i], self._data.mags[i])
            self.confline[i].set_data(self._data.freqs[i], self._data.conf[i])

            (x,y) = self._data.peakxy[i]
            # peak position
            self.annotation[i].xy = (x,y)
            self.annotation[i].set(text= ("cps %.2f" % (1/x)))
            # annotation position
            self.annotation[i].set_position((x+0.1,y+0.1))
            
            self.sigline[i].axes.relim()
            self.sigline[i].axes.autoscale_view()
            self.fftline[i].axes.relim()
            self.fftline[i].axes.autoscale_view()
            self.confline[i].axes.relim()
            self.confline[i].axes.autoscale_view()

class Fetch(threading.Thread):

    def __init__(self, data):

        threading.Thread.__init__(self)

        self._data = data
        self._period = 0.25
        self._nextCall = time.time()

    def run(self):
        times = [time.time()]        
        while True:
            osc_server.recv(1000)
            times.append(time.time())
            # TODO - divide by two if there's two streams of messages
            samplerate = 1/ ((times[-1] - times[0]) / len(times))            
            if len(times) > 100:
                times.pop(0)
            #print("samplerate %.2f" % (samplerate))
            
data = Data()
plotter = Plot(data)
fetcher = Fetch(data)

# TODO - calculate samplerate rather than trusting global
def sensor_callback(path, args):
    m = re.match("/(\w+)/imu", path)
    name = m.groups()[0]
    distance = 0
    if len(prev[name]) > 0:
        # calculate distance in euclidean space
        for i in range(0, dimensions):
            p = args[i] - prev[name][i]
            distance = distance + (p * p)
        distance = math.sqrt(distance)
        speed = distance/samplerate
        
    incoming(fetcher, name, (args[0] + 256) / 512)
    
    prev[name] = args

for name in names:
    osc_server.add_method("/" + name + "/imu", "ffffffffff", sensor_callback)

def fallback(path, args, types, src):
    print("got unknown message '%s' from '%s'" % (path, src.url))
    for a, t in zip(args, types):
        print("argument of type '%s': %s" % (t, a))
osc_server.add_method(None, None, fallback)

fetcher.start()
plt.show()
#fetcher.join()
